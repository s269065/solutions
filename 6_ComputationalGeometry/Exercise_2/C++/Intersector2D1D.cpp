#include "Intersector2D1D.h"

Intersector2D1D::Intersector2D1D()
{
    toleranceParallelism = 1.0E-7;
    toleranceIntersection = 1.0E-7;
    intersectionType = Intersector2D1D::NoInteresection;
    intersectionParametricCoordinate = 0;
}
Intersector2D1D::~Intersector2D1D()
{

}

void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    _planeNormal = planeNormal;
    _planeTranslation = planeTranslation;
    return;
}

void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
    _lineOrigin = lineOrigin;
    _lineTangent = lineTangent;
    return;
}

bool Intersector2D1D::ComputeIntersection()
{
    intersectionType = NoInteresection;
    bool intersection = false;
    check1 = 0;
    check2 = 0;

    for (unsigned int i = 0; i < 3; i++)
        check1 += _planeNormal(i) * _lineTangent(i);
    if (abs(check1) > toleranceIntersection)
    {
        intersectionType = PointIntersection;
        intersection = true;
    }

    for (unsigned int i = 0; i < 3; i++)
        check2 += _planeNormal(i) * _lineOrigin(i);
    if (abs(check1) < toleranceParallelism)
    {
        if (_planeTranslation == check2)
            intersectionType = Coplanar;
        else
            intersectionType = NoInteresection;
    }

    return intersection;
}

Vector3d Intersector2D1D::IntersectionPoint()
{
    intersectionParametricCoordinate = (_planeTranslation - check2) / check1;
    Vector3d intersection;
    for (unsigned int i = 0; i < 3; i++)
        intersection(i) += _lineOrigin(i) + intersectionParametricCoordinate * _lineTangent(i);
    return intersection;
}
