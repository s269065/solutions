#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

  Ellipse::Ellipse(const Point& center, const double& a, const double& b)
  {
      _center = center;
      _a = a;
      _b = b;
  }

  double Ellipse::Perimeter() const
  {
      double perimeter = 2*M_PI*sqrt((pow(_a,2)+pow(_b,2))*0.5);
      return perimeter;
  }

  Circle::Circle(const Point& center, const double& radius) : Ellipse(center, radius, radius) { }

  Triangle::Triangle(const Point& p1, const Point& p2, const Point& p3)
  {
      l1 = (p1-p2).ComputeNorm2();
      l2 = (p2-p3).ComputeNorm2();
      l3 = (p3-p1).ComputeNorm2();
  }

  void Triangle::AddVertex(const Point &point)
  {
      if (points.size() < 3)
          points.push_back(point);
  }

  double Triangle::Perimeter() const
  {
    double perimeter = l1 + l2 + l3;
    return perimeter;
  }

  TriangleEquilateral::TriangleEquilateral(const Point& p1, const double& edge)
  {
      _p1 = p1;
      l1 = edge;
      l2 = edge;
      l3 = edge;
  }

  Quadrilateral::Quadrilateral(const Point& p1, const Point& p2, const Point& p3, const Point& p4)
  {
      l1 = (p1-p2).ComputeNorm2();
      l2 = (p2-p3).ComputeNorm2();
      l3 = (p3-p4).ComputeNorm2();
      l4 = (p4-p1).ComputeNorm2();

  }

  void Quadrilateral::AddVertex(const Point &p)
  {
     if (points.size() < 4)
         points.push_back(p);
  }

  double Quadrilateral::Perimeter() const
  {
    double perimeter = l1 + l2 + l3 + l4;
    return perimeter;
  }

  Rectangle::Rectangle(const Point& p1, const Point& p2, const Point& p3, const Point& p4) : Quadrilateral(p1, p2, p3, p4) {}

  Rectangle::Rectangle(const Point& p1, const double& base, const double& height)
  {
      l1 = base;
      l2 = height;
      l3 = base;
      l4 = height;
  }

  Square::Square(const Point& p1, const Point& p2, const Point& p3, const Point& p4) : Rectangle(p1, p2, p3, p4) {}

  Square::Square(const Point& p1, const double& edge)
  {
      l1 = edge;
      l2 = edge;
      l3 = edge;
      l4 = edge;
  }

  Point::Point(const double &x, const double &y)
  {
      X = x;
      Y = y;
  }

  double Point::ComputeNorm2() const
  {
      Point p(X,Y);
      return sqrt(pow(p.X, 2) + pow(p.Y, 2));
  }

  Point Point::operator+(const Point& point) const
  {
      Point tempPoint(X,Y);
      tempPoint.X = X + (point.X);
      tempPoint.Y = Y + (point.Y);
      return tempPoint;
  }

  Point Point::operator-(const Point& point) const
  {
      Point tempPoint(X,Y);
      tempPoint.X = X - (point.X);
      tempPoint.Y = Y - (point.Y);
      return tempPoint;
  }

  Point&Point::operator-=(const Point& point)
  {
      X -= point.X;
      Y -= point.Y;
      return *this;
  }

  Point&Point::operator+=(const Point& point)
  {
      X += point.X;
      Y += point.Y;
      return *this;
  }
}
