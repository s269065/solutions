# Car Factory

On a car brochure, a car is identified by the following properties:

* Color
* Producer
* Model

## Example 

See the image below as an example of three different cars

![car](Images/car.png)

## Requirements

Write a software which emulate a car factory who produces a car on the brochure.

### Advance

Starting from the second practice exercise we implement some useful features:

![car_cd3](Images/car_cd3.png)
