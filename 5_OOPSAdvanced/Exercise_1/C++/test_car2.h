#ifndef __TEST_CAR2_H
#define __TEST_CAR2_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>
#include "algorithm"
#include "car2.h"

using namespace testing;

TEST(TestCar2, TestShow)
{
  CarLibrary2::Car car = CarLibrary2::Car("Ford", "Mustang", "Red");
  car.SetPower(5.0);
  EXPECT_EQ(car.Show(), "Mustang (Ford): color Red");
}

TEST(TestCarFactory2, TestCreateFord)
{
  CarLibrary2::Car* car = CarLibrary2::Ford().Create("Red");
  CarLibrary2::Car* car2 = CarLibrary2::Ford().Create("Blue", "Fiesta");

  EXPECT_TRUE(car != nullptr);
  EXPECT_TRUE(car2 != nullptr);

  stringstream buffer;
  // Save cout's buffer here
  streambuf *sbuf = cout.rdbuf();
  // Redirect cout to our stringstream buffer or any other ostream
  cout.rdbuf(buffer.rdbuf());
  // Use cout as usual
  cout << *car;
  EXPECT_EQ(buffer.str(), "Mustang (Ford): color Red\n");
  // When done redirect cout to its old self
  cout.rdbuf(sbuf);


  EXPECT_EQ(car2->Show(), "Fiesta (Ford): color Blue");

  set<CarLibrary2::Car > setCar;

  car->SetPower(10);
  car2->SetPower(5.0);

  CarLibrary2::Car car3 = *car + *car2;
  CarLibrary2::Car car4 = *car2 + *car;
  CarLibrary2::Car car5 = *car2 + 10;

  vector<CarLibrary2::Car> cars;
  cars.reserve(5);
  cars.push_back(car3);
  cars.push_back(car5);
  cars.push_back(car4);
  sort(cars.begin(), cars.end());

  *car += car3;
  CarLibrary2::Car& carReference = *car;
  CarLibrary2::Car& carReference2 = *car2;

  bool check2 = (carReference < carReference2);

  setCar.insert(*car);
  setCar.insert(*car2);


  delete car;
  delete car2;
}
#endif // __TEST_CAR2_H
