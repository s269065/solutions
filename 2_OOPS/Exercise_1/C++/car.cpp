#include "car.h"

namespace CarLibrary {

Car::Car(const string &producer,
         const string &model,
         const string &color)
{
    _producer = producer;
    _model = model;
    _color = color;
}

string Car::Show()
{
    return _model + " (" + _producer + "): color " + _color;
}

CarProducer CarFactory::_producer = CarProducer::UNKNOWN;

void CarFactory::StartProduction(const CarProducer &producer)
{
    _producer = producer;
}

Car* CarFactory::Create(const string &color) //* = puntatore a variabile di tipo car
{
     switch (_producer)
     {
        case CarProducer::FORD:
         return new Car("Ford", "Mustang", color);
        case CarProducer::TOYOTA:
         return new Car("Toyota", "Prius", color);
        case CarProducer::VOLKSWAGEN:
         return new Car("Volkswagen", "Golf", color);
        default:
         throw runtime_error("Invalid producer");
     }

}

}
