#ifndef CAR_H
#define CAR_H

#include <iostream>

using namespace std;

namespace CarLibrary {

  class Car
  {
    private:
      string _producer;
      string _model;
      string _color;

    public:
      Car(const string& producer,
          const string& model,
          const string& color);

      string Show();

  };


  enum class CarProducer
  {
    UNKNOWN = 0,
    FORD = 1,
    TOYOTA = 2,
    VOLKSWAGEN = 3
  };

  class CarFactory
  {
    private:
      static CarProducer _producer;

    public:
      static void StartProduction(const CarProducer& producer);
      static Car* Create(const string& color);
  };
}

#endif // CAR_H
