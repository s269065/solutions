from enum import Enum


class Car:
    def __init__(self, producer: str, model: str, color: str):
        self.__producer = producer # __ vuol dire privato
        self.__model = model
        self.__color = color

    def show(self) -> str:
        return self.__model + " (" + self.__producer + "): color " + self.__color


class CarProducer(Enum):
    UNKNOWN = 0
    FORD = 1
    TOYOTA = 2
    VOLKSWAGEN = 3


class CarFactory:
    __producer = CarProducer.UNKNOWN

    @staticmethod
    def start_production(producer: CarProducer):
        CarFactory.__producer = producer #al di fuori del costruttore variabili statiche, non serve il self

    @staticmethod
    def create(color: str) -> Car:
        if CarFactory.__producer == CarProducer.FORD: #non c'è lo switch in python
            return Car("Ford", "Mustang", color)
        elif CarFactory.__producer == CarProducer.TOYOTA:
            return Car("Toyota", "Prius", color)
        elif CarFactory.__producer == CarProducer.VOLKSWAGEN:
            return Car("Volkswagen", "Golf", color)
        else:
            raise ValueError("Invalid producer")
