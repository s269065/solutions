class Car:
    def __init__(self, producer: str, model: str, color: str):
        pass

    def show(self) -> str:
        return ""


class ICarFactory:
    def create(self, color: str) -> Car:
        pass


class Ford(ICarFactory):
    def create(self, color: str) -> Car:
        return Car("Ford", "Mustang", color)


class Toyota(ICarFactory):
    def create(self, color: str) -> Car:
        return Car("Toyota", "Prius", color)


class Volkswagen(ICarFactory):
    def create(self, color: str) -> Car:
        return Car("Volkswagen", "Golf", color)
