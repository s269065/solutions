#include <iostream>
#include "Eigen"

using namespace std;
using namespace Eigen;


/// \brief shape(n,m) is the n-by-m matrix with elements from 1 to n * m.
MatrixXd Shape(const int& n,
               const int& m = 0);
/// \brief rand(n,m) is the n-by-m matrix with random elements.
MatrixXd Rand(const int& n,
              const int& m = 0);
/// \brief hilb(n,m) is the n-by-m matrix with elements 1/(i+j-1).
MatrixXd Hilb(const int& n,
              const int& m = 0);
///
/// \brief solveSystem(A) solve the linear system Ax=b with x ones.
/// \param detA the determinant of A
/// \param condA the condition number of A
/// \param errRel the realtive error
void SolveSystem(const MatrixXd& A,
                 double& detA,
                 double& condA,
                 double& errRel);

int main()
{
  int n = 4;

  double detAS, condAS, errRelS;
  SolveSystem(Shape(n, n), detAS, condAS, errRelS);
  cout<< scientific<< "shape - DetA: "<< detAS<< ", RCondA: "<< 1.0 / condAS<< ", Relative Error: "<< errRelS<< endl;

  double detAR, condAR, errRelR;
  SolveSystem(Rand(n, n), detAR, condAR, errRelR);
  cout<< scientific<< "rand - DetA: "<< detAR<< ", RCondA: "<< 1.0 / condAR<< ", Relative Error: "<< errRelR<< endl;

  double detAH, condAH, errRelH;
  SolveSystem(Hilb(n, n), detAH, condAH, errRelH);
  cout<< scientific<< "hilb - DetA: "<< detAH<< ", RCondA: "<< 1.0 / condAH<< ", Relative Error: "<< errRelH<< endl;

  return 0;
}


MatrixXd Shape(const int& n, const int& m)
{
  MatrixXd A = MatrixXd::Zero(n,m);

  int counter = 1;
  for(int i = 0; i < n; i++)
  {
      for(int j = 0; j < n; j++)
      {
          A(i,j) = counter++;
      }
  }

  return A;
}

MatrixXd Rand(const int& n, const int& m)
{
  return MatrixXd::Random(n,m);
}


MatrixXd Hilb(const int& n, const int& m)
{
    MatrixXd A = MatrixXd::Zero(n,m);

    for(int i = 0; i < n; i++)
    {
        for(int j = 0; j < n; j++)
        {
            A(i,j) = 1.0 / (1.0 + i + j);
        }
    }
  return MatrixXd();
}

void SolveSystem(const MatrixXd& A,
                 double& detA,
                 double& condA,
                 double& errRel)
{
  int n = A.rows();
  VectorXd solution = VectorXd::Ones(n);
  VectorXd b = A.rowwise().sum();

  VectorXd x;
  x = A.fullPivLu().solve(b);

  detA = A.determinant();

  JacobiSVD<MatrixXd> svd(A); //template con <>
  condA = svd.singularValues()[0] / svd.singularValues()[n-1];

  errRel = (solution - x).norm() / solution.norm();
}
