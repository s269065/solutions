# \brief ImportVectors reads the input vectors from file for dot product
# \param filePath: path name of the input file
# \return the result of the reading, true is success, false is error
# \return n: resulting size of the vectors
# \return v1: resulting vector1
# \return v2: resulting vector2
def importVectors(inputFilePath):
    file = open(inputFilePath, 'r')
    lines = file.readlines()

    n = int(lines[1])
    v1 = [ int(s) for s in lines[3].split(' ') ] #rendi int i numeri in v1 separati da uno spazio
    v2 = [int(s) for s in lines[5].split(' ')]

    file.close()

    return True, n, v1, v2

# \brief DotProduct performs the dot product between two vectors
# \param n: size of the vectors
# \param v1: the first vector
# \param v2: the second vector
# \return the result of the operation, true is success, false is error
# \return dotProduct: the resulting dot product
def dotProduct(n, v1, v2):
    result = 0;
    for i in range(0, n):
        result += v1[i] * v2[i]

    return True, result

# \brief ExportResult export the result obtained in file
# \param outputFilePath: path name of the output file
# \param v1: vector1
# \param v2: vector2
# \param dotProduct: the dot product
# \return the result of the export, true is success, false
def exportResult(outputFilePath, n, v1, v2, dotProduct):
    file = open(outputFilePath, 'w')
    print("# Size of the two vectors", file=file)
    print(n, file=file)
    print("# vector 1", file=file)
    print(*v1, file=file)
    print("# vector 2", file=file)
    print(*v2, file=file)
    print("# dot product", file=file)
    print(dotProduct, file=file)

    file.close()

    return True


if __name__ == '__main__':
    inputFileName = "vectors.txt"

    [resultImport, n, v1, v2] = importVectors(inputFileName)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1)
    else:
        print("Import successful: n=", n, " v1=", v1, " v2=", v2)

    [resultDotProduct, dotProduct] = dotProduct(n, v1, v2)
    if not resultDotProduct:
        print("Something goes wrong with dot product")
        exit(-1)
    else:
        print("Computation successful: result ", dotProduct)

    outputFileName = "dotProduct.txt"
    if not exportResult(outputFileName, n, v1, v2, dotProduct):
        print("Something goes wrong with export")
        exit(-1)
    else:
        print("Export successful")
