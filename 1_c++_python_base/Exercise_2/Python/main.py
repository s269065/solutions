import sys

# \brief ImportText import the text for encryption
# \param inputFilePath: the input file path
# \return the result of the operation, true is success, false is error
# \return text: the resulting text
def importText(inputFilePath):
    file = open(inputFilePath, 'r')
    text = file.readline()
    file.close()
    return True, text

# \brief Encrypt encrypt the text
# \param text: the text to encrypt
# \param password: the password for encryption
# \return the result of the operation, true is success, false is error
# \return encryptedText: the resulting encrypted text
def encrypt(text, password):
    encryptedText = ""  # inizializzazione stringa
    cont = 0
    for i in range(0, len(text)):
        for j in range(0, len(password)):
            if j == i - cont * len(password):
                encryptedText += chr(ord(text[i]) + ord(password[j]))
        if i == cont*len(password)+(len(password) - 1):
            cont += 1

    return True, encryptedText

# \brief Decrypt decrypt the text
# \param text: the text to decrypt
# \param password: the password for decryption
# \return the result of the operation, true is success, false is error
# \return decryptedText: the resulting decrypted text
def decrypt(text, password):
    decryptedText = ""
    cont = 0
    for i in range(0, len(text)):
        for j in range(0, len(password)):
            if j == i - cont * len(password):
                decryptedText += chr(ord(text[i]) - ord(password[j]))
        if i == cont * len(password) + (len(password) - 1):
            cont += 1

    return True, decryptedText

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Password shall passed to the program")
        exit(-1)
    password = sys.argv[1]

    inputFileName = "text.txt"

    [resultImport, text] = importText(inputFileName)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1)
    else:
        print("Import successful: text=", text)

    [resultEncrypt, encryptedText] = encrypt(text, password)
    if not resultEncrypt:
        print("Something goes wrong with encryption")
        exit(-1)
    else:
        print("Encryption successful: result= ", encryptedText)

    [resultEncrypt, decryptedText] = decrypt(encryptedText, password)
    if not resultEncrypt or text != decryptedText:
        print("Something goes wrong with decryption")
        exit(-1)
    else:
        print("Decryption successful: result= ", decryptedText)
