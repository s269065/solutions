#ifndef SHOPPING_H
#define SHOPPING_H

#include <iostream>
#include <vector> //parentesi uncinate per includere una libreria standard (non creata da noi)
#include <list>
#include <queue>
#include <stack>
#include <unordered_set>

using namespace std;

namespace ShoppingLibrary {

  class IShoppingApp {
    public:
      virtual unsigned int NumberElements() const = 0;
      virtual void AddElement(const string& product) = 0;
      virtual void Undo() = 0;
      virtual void Reset() = 0;
      virtual bool SearchElement(const string& product) = 0;
  };

  class VectorShoppingApp : public IShoppingApp {
    private:
      vector<string> elements;

    public:
      unsigned int NumberElements() const { return elements.size(); }
      void AddElement(const string& product) { elements.push_back(product); } //per aggiungere elementi in coda
      void Undo() { elements.pop_back(); } //per eliminare l'ultimo elemento inserito
      void Reset() { elements.clear(); } //pulisce e ridimensiona il vettore a zero
      bool SearchElement(const string& product) {
          unsigned int numElements = NumberElements();
          for (unsigned int i= 0; i < numElements; i++)
          {
              if (elements[i] == product)
                  return true;
          }
          return false;
      }
  };

  class ListShoppingApp : public IShoppingApp {
    private:
      list<string> elements;

    public:
      unsigned int NumberElements() const { return elements.size(); }
      void AddElement(const string& product) { return elements.push_back(product); }
      void Undo() { elements.pop_back(); }
      void Reset() { elements.clear(); }
      bool SearchElement(const string& product) {
          for(list<string>::const_iterator it = elements.begin(); it != elements.end(); it++) //iterator = metodo statico (::)
          {
            // const string& element = *it;
            // const string* pElement = it; si può fare anche così
            if (*it == product)
                    return true;
          }
          return false;
      }
  };

  class QueueShoppingApp : public IShoppingApp {
    private:
      queue<string> elements;

    public:
      unsigned int NumberElements() const { elements.size(); }
      void AddElement(const string& product) { elements.push(product); }
      void Undo() {
          queue<string> undoQueue; //ulteriore coda
          unsigned int numElements = NumberElements();
          for(unsigned int i=0; i < numElements - 1; i++)
          {
              undoQueue.push(elements.back());
              elements.pop();
          }
          elements.pop();
          for(unsigned int i=0; i < numElements - 1; i++)
          {
              AddElement(undoQueue.back());
              undoQueue.pop();
          }
      }
      void Reset() {
          unsigned int numElements = NumberElements();
          for(unsigned int i=0; i < numElements - 1; i++)
              elements.pop();
      }
      bool SearchElement(const string& product) {
          queue<string> undoQueue;
          bool found = false;
          unsigned int numElements = NumberElements();
          for(unsigned int i=0; i < numElements - 1; i++)
          {
              const string& element = elements.back();
              if (element == product)
              {
                  found = true;
                  break;
              }
              undoQueue.push(element);
              elements.pop();
          }
          unsigned int numElementsUndo = undoQueue.size();
          for(unsigned int i=0; i < numElementsUndo; i++)
          {
              AddElement(undoQueue.back());
              undoQueue.pop();
          }
          return found;
      }
  };

  class StackShoppingApp : public IShoppingApp {
    private:
      stack<string> elements;

    public:
      unsigned int NumberElements() const { elements.size(); }
      void AddElement(const string& product) { elements.push(product); }
      void Undo() { elements.pop(); }
      void Reset() {
          unsigned int numElements = NumberElements();
          for (unsigned int i=0; i < numElements; i++)
              Undo();
      }
      bool SearchElement(const string& product) {
          stack<string> undoStack;
          bool found = true;
          unsigned int numElements = NumberElements();
          for (unsigned int i=0; i < numElements; i++)
          {
              string& element = elements.top();
              if (element == product)
              {
                  found = true;
                  break;
              }
              undoStack.push(element);
              elements.pop();
          }
          unsigned int numElementUndo = undoStack.size();
          for (unsigned int i=0; i < numElementUndo; i++)
          {
              AddElement(undoStack.top());
              undoStack.pop();
          }
          return found;
      }
  };

  class HashShoppingApp : public IShoppingApp {
    private:
      string lastElement;
      unordered_set<string> elements;

    public:
      unsigned int NumberElements() const { return elements.size(); }
      void AddElement(const string& product) { lastElement = product; elements.insert(product); }
      void Undo() {elements.erase(lastElement); }
      void Reset() { elements.clear(); }
      bool SearchElement(const string& product) { return elements.find(product) != elements.end(); }
  };
}

#endif // SHOPPING_H
