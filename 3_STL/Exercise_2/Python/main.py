class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description


class Pizza:
    def __init__(self, name: str):
        self.Name = name
        self.pizzaIngredients = []

    def addIngredient(self, ingredient: Ingredient):
        self.pizzaIngredients.append(ingredient)
        pass

    def numIngredients(self) -> int:
        return len(self.pizzaIngredients)

    def computePrice(self) -> int:
        price = 0
        for i in range(0, self.numIngredients()):
            price += self.pizzaIngredients[i].Price
        return price


class Order:
    def __init__(self):
        self.ordPizzas = []  # pizza ordinate

    def numPizzas(self) -> int:
        return len(self.ordPizzas)

    def initializeOrder(self, numPizzas: int):
        self.ordPizzas = [0] * numPizzas

    def addPizza(self, pizza: Pizza):
        self.ordPizzas.append(pizza)

    def getPizza(self, position: int) -> Pizza:
        numPizzas = self.numPizzas()
        if position > numPizzas or position < 0:
            raise Exception("Position passed is wrong")
        pizza = self.ordPizzas[position - 1]
        return pizza

    def computeTotal(self) -> int:
        price = 0
        for pizza in self.ordPizzas:
            price += pizza.computePrice()
        return price


class Pizzeria:
    def __init__(self):
        self.ingredients = []  # ingredienti presenti in cucina
        self.listIng = []  # lista ingredienti ordinati
        self.pizMenu = []
        self.orders = []

    def addIngredient(self, name: str, description: str, price: int):
        for newIng in self.ingredients:
            if name == newIng.Name:
                raise Exception("Ingredient already inserted")
        newIng = Ingredient(name, price, description)
        self.ingredients.append(newIng)

    def findIngredient(self, name: str) -> Ingredient:
        for ingredient in self.ingredients:
            if name == ingredient.Name:
                return ingredient
        raise Exception("Ingredient not found")

    def addPizza(self, name: str, ingredients: []):
        for newPizza in self.pizMenu:
            if name == newPizza.Name:
                raise Exception("Pizza already inserted")
        newPizza = Pizza(name)
        for ingPizza in ingredients:
            for ingredient in self.ingredients:
                if ingPizza == ingredient.Name:
                    newPizza.pizzaIngredients.append(ingredient)
        self.pizMenu.append(newPizza)

    def findPizza(self, name: str) -> Pizza:
        for pizza in self.pizMenu:
            if name == pizza.Name:
                return pizza
        raise Exception("Pizza not found")

    def createOrder(self, pizzas: []) -> int:
        if len(pizzas) == 0:
            raise Exception("Empty order")
        order = Order()
        for i in range(0, len(pizzas)):
            for pizza in self.pizMenu:
                if pizza.Name == pizzas[i]:
                    order.ordPizzas.append(pizza)
        order.numOrd = len(self.orders) + 1000
        self.orders.append(order)
        return order.numOrd

    def findOrder(self, numOrder: int) -> Order:
        for order in self.orders:
            if numOrder == order.numOrd:
                return order
        raise Exception("Order not found")

    def getReceipt(self, numOrder: int) -> str:
        receipt = ''
        for order in self.orders:
            if numOrder == order.numOrd:
                for pizza in order.ordPizzas:
                    receipt += "- " + pizza.Name + ", " + str(pizza.computePrice()) + " euro" + "\n"
                receipt += "  TOTAL: " + str(order.computeTotal()) + " euro" + "\n"
                return receipt
        raise Exception("Order not found")

    def listIngredients(self) -> str:
        list = ''
        temp = []
        for ingredient in self.ingredients:
            temp.append(ingredient.Name)
        temp.sort()
        for ing in temp:
            for ingredient in self.ingredients:
                if ingredient.Name == ing:
                    self.listIng.append(ingredient)
        for i in range(0, len(self.listIng)):
            list += self.listIng[i].Name + " - '" + self.listIng[i].Description + "': " + str(self.listIng[i].Price) + " euro" + "\n"
        return list

    def menu(self) -> str:
        Menu = ''
        for p in self.pizMenu:
            Menu += p.Name + " (" + str(p.numIngredients()) + " ingredients): " + str(p.computePrice()) + " euro" + "\n"
        return Menu
