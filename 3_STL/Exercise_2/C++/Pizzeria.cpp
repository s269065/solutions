#include "Pizzeria.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <list>
#include <sstream>

namespace PizzeriaLibrary {
string Name;
int Price;
string Description;

void Pizza::AddIngredient(const Ingredient& ingredient) { pizzaIngredients.push_back(ingredient); }

int Pizza::NumIngredients() const { return pizzaIngredients.size(); }

int Pizza::ComputePrice() const {
    unsigned int price = 0;
    unsigned int numIngredients = NumIngredients();
    for(unsigned int i = 0; i < numIngredients; i++)
        price += pizzaIngredients[i].Price;
    return price; }

void Order::InitializeOrder(int numPizzas) { ordPizzas.reserve(numPizzas); }

void Order::AddPizza(const Pizza& pizza) { ordPizzas.push_back(pizza); }

int Order::NumPizzas() const { return ordPizzas.size(); }

const Pizza& Order::GetPizza(const int& position) const {
    const int numPizzas = NumPizzas();
    if(position > numPizzas || position < 0)
        throw runtime_error("Position passed is wrong");
    const Pizza& pizza = ordPizzas[position-1];
    return pizza;
}

int Order::ComputeTotal() const {
    unsigned int price = 0;
    unsigned int numPizzas = NumPizzas();
    for (unsigned int i = 0; i < numPizzas; i++){
        price += ordPizzas[i].ComputePrice();
    }
    return price;
}

void Pizzeria::AddIngredient(const string& name, const string& description, const int& price)
{
    Ingredient newIng;
    for (unsigned int i = 0; i < allIng.size(); i++){
        if(name == allIng[i].Name)
            throw runtime_error("Ingredient already inserted");
    }
    newIng.Name = name;
    newIng.Description = description;
    newIng.Price = price;
    allIng.push_back(newIng);
    sort(allIng.begin(), allIng.end()); //ingredienti in ordine alfabetico
}

const Ingredient& Pizzeria::FindIngredient(const string& name) const
{
    for (unsigned int i = 0; i < allIng.size(); i++)
    {
        if (name == allIng[i].Name)
            return allIng[i];
    }
    throw runtime_error("Ingredient not found");

}

void Pizzeria::AddPizza(const string& name, const vector<string>& ingredients)
{
    for (unsigned int i = 0; i < pizMenu.size(); i++)
    {
        if(name == pizMenu[i].Name)
            throw runtime_error("Pizza already inserted");
    }
    Pizza newPiz;
    newPiz.Name = name;
    for (unsigned int i = 0; i < ingredients.size(); i++)
    {
        for (unsigned int j = 0; j < allIng.size(); j++)
        {
            if (ingredients[i] == allIng[j].Name)
                newPiz.pizzaIngredients.push_back(allIng[j]);

        }
    }
    pizMenu.push_back(newPiz);
}

const Pizza& Pizzeria::FindPizza(const string& name) const
{
    for (unsigned int i = 0; i < pizMenu.size(); i++)
    {
        if (name == pizMenu[i].Name)
            return pizMenu[i];
    }
    throw runtime_error("Pizza not found");
}

int Pizzeria::CreateOrder(const vector<string>& pizzas) {
    if (pizzas.size() == 0)
        throw runtime_error("Empty order");
    Order order;
    for (unsigned int i = 0; i < pizzas.size(); i++)
    {
        for (unsigned int j = 0; j < pizMenu.size(); j++)
        {
            if (pizzas[i] == pizMenu[j].Name)
                order.ordPizzas.push_back(pizMenu[j]);
        }
    }
    order.numOrd = orders.size() + 1000;
    orders.push_back(order);
    return order.numOrd;
}

const Order& Pizzeria::FindOrder(const int& numOrder) const {
    for (unsigned int i = 0; i < orders.size(); i++)
    {
        if (numOrder == orders[i].numOrd)
            return orders[i];
    }
    throw runtime_error("Order not found");
}

string Pizzeria::GetReceipt(const int& numOrder) const {
    string receipt;
    for (unsigned int i = 0; i < orders.size(); i++)
    {
        if (numOrder == orders[i].numOrd)
        {
            for (int j = 0; j < orders[i].NumPizzas(); j++)
                receipt += "- " + orders[i].ordPizzas[j].Name + ", " + to_string(orders[i].ordPizzas[j].ComputePrice()) + " euro" + "\n";
        }
        receipt += "  TOTAL: " + to_string(orders[i].ComputeTotal()) + " euro" + "\n";
        return receipt;
    }
    throw runtime_error("Order not found");
}

string Pizzeria::ListIngredients() const {
    string list;
    for (unsigned int i = 0; i < allIng.size(); i++)
        list += allIng[i].Name + " - '" + allIng[i].Description + "': " + to_string(allIng[i].Price) + " euro" + "\n";
    return list;
}

string Pizzeria::Menu() const {
    string Menu;
    for (int i = (pizMenu.size() - 1); i > -1 ; i--) //per ritornare in ordine inverso all'inserimento
        Menu += pizMenu[i].Name + " (" + to_string(pizMenu[i].NumIngredients()) + " ingredients): " + to_string(pizMenu[i].ComputePrice()) + " euro" + "\n";
    return Menu;
}


}
